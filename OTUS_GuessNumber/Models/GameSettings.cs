﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OTUS_GuessNumber.Models
{
    public class GameSettings
    {
        public GameSettings(int minValue, int maxValue, int countOfAttempts)
        {
            MinValue = minValue;
            MaxValue = maxValue;
            CountOfAttempts = countOfAttempts;
        }

        public int MinValue { get; init; }
        public int MaxValue { get; init; }
        public int CountOfAttempts { get; init; }
    }
}
