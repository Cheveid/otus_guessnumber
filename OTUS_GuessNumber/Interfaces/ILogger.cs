﻿namespace OTUS_GuessNumber.Interfaces
{
    public interface ILogger
    {
        void Log(string message);
    }
}
