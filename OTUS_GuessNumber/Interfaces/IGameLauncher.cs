﻿using OTUS_GuessNumber.Interfaces;

namespace OTUS_GuessNumber.Implementations
{
    public interface IGameLauncher
    {
        void Run(Game game);
    }
}