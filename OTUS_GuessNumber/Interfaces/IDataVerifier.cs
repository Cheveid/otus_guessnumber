﻿namespace OTUS_GuessNumber.Interfaces
{
    public interface IDataVerifier
    {
        bool Verify(int hiddenNumber, int verifyNumber, out bool isLess);
    }
}
