﻿namespace OTUS_GuessNumber.Interfaces
{
    public interface IDataGenerator
    {
        int Generate(int min, int max);
    }
}
