﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace OTUS_GuessNumber.Interfaces
{
    public abstract class Game
    {
        public delegate void DoEvent(string message);
        public event DoEvent OnDoEvent;

        protected void RaiseOnDoEvent(string message)
        {
            OnDoEvent?.Invoke(message);
        }

        public abstract void Start();
    }
}
