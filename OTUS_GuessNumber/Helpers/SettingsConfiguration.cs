﻿using OTUS_GuessNumber.Models;
using System.Configuration;

namespace OTUS_GuessNumber.Helpers
{
    public class SettingsConfiguration
    {
        public static GameSettings Init()
        {
            int countOfAttempts = int.Parse(ConfigurationManager.AppSettings["CountOfAttempts"]?.ToString() ?? "10");
            int minValue = int.Parse(ConfigurationManager.AppSettings["MinValue"]?.ToString() ?? "0");
            int maxValue = int.Parse(ConfigurationManager.AppSettings["MaxValue"]?.ToString() ?? "10");
            return new GameSettings(minValue, maxValue, countOfAttempts);
        }
    }
}
