﻿using OTUS_GuessNumber.Helpers;
using OTUS_GuessNumber.Implementations;
using OTUS_GuessNumber.Models;

namespace OTUS_GuessNumber
{
    class Program
    {
        static void Main(string[] args)
        {
            GameSettings gameSettings = SettingsConfiguration.Init();
            DataGenerator dataGenerator = new DataGenerator();
            DataVerifier dataVerifier = new DataVerifier();

            GuessGame guessGame = new GuessGame(gameSettings, dataGenerator, dataVerifier);

            FileLogger fileLogger = new FileLogger("GameLog.txt");
             
            GameLauncher gameLauncher = new GameLauncher(fileLogger);
            
            gameLauncher.Run(guessGame);
        }
    }
}
