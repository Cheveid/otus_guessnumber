﻿using OTUS_GuessNumber.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace OTUS_GuessNumber.Implementations
{
    public class DataVerifier : IDataVerifier
    {
        public bool Verify(int hiddenNumber, int verifyNumber, out bool isLess)
        {
            isLess = verifyNumber < hiddenNumber;
            return verifyNumber == hiddenNumber;
        }
    }
}
