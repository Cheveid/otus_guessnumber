﻿using OTUS_GuessNumber.Interfaces;

namespace OTUS_GuessNumber.Implementations
{
    public class GameLauncher : IGameLauncher
    {
        private Game _game;
        protected readonly ILogger _logger;

        public GameLauncher(ILogger logger)
        {
            _logger = logger;
        }

        public void Run(Game game)
        {
            _game = game;
            _game.OnDoEvent += Game_OnDoEvent;

            _game.Start();
        }

        private void Game_OnDoEvent(string message)
        {
            _logger.Log(message);
        }
    }
}
