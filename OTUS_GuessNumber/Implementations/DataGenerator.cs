﻿using OTUS_GuessNumber.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace OTUS_GuessNumber.Implementations
{
    public class DataGenerator : IDataGenerator
    {
        public int Generate(int min, int max)
        {
            return new Random().Next(min, max);
        }
    }
}
