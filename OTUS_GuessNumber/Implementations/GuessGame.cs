﻿using OTUS_GuessNumber.Interfaces;
using OTUS_GuessNumber.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace OTUS_GuessNumber.Implementations
{
    public class GuessGame : Game
    {
        private readonly IDataGenerator _dataGenerator;
        private readonly GameSettings _gameSettings;
        private readonly IDataVerifier _dataVerifier;

        public GuessGame(GameSettings gameSettings, IDataGenerator dataGenerator, IDataVerifier dataVerifier)
        {
            _dataGenerator = dataGenerator;
            _gameSettings = gameSettings;
            _dataVerifier = dataVerifier;
        }

        public override void Start()
        {
            bool isExit = false;
            while (!isExit)
            {
                Console.Clear();
                Console.WriteLine("Меню:");
                Console.WriteLine("1. Начать новую игру");
                Console.WriteLine("0. Выход (Esc)");
                Console.WriteLine();
                Console.Write("Ваш выбор: ");
                var key = Console.ReadKey();
                Console.Clear();

                switch (key.Key)
                {
                    case ConsoleKey.D1:
                    case ConsoleKey.NumPad1:
                        StartNewGame();
                        break;

                    case ConsoleKey.Escape:
                    case ConsoleKey.D0:
                    case ConsoleKey.NumPad0:
                        RaiseOnDoEvent("Выход из игры");
                        isExit = true;
                        break;
                }

                if (!isExit)
                {
                    Console.WriteLine();
                    Console.WriteLine($"Для продолжения нажмите любую клавишу...");
                    Console.ReadKey();
                }
            }
        }

        private void StartNewGame()
        {
            RaiseOnDoEvent("Новая игра началась");
            Console.WriteLine($"Вы должны угадать число от {_gameSettings.MinValue} до {_gameSettings.MaxValue}");
            Console.WriteLine();

            int hiddenNumber = _dataGenerator.Generate(_gameSettings.MinValue, _gameSettings.MaxValue);

            RaiseOnDoEvent($"Загаданное число: {hiddenNumber}, кол-во попыток: {_gameSettings.CountOfAttempts}");

            int step = 1;
            string message;
            bool isWin = false;
            while (step <= _gameSettings.CountOfAttempts)
            {
                Console.Write($"Попытка №{step} из {_gameSettings.CountOfAttempts}. Ваше число: ");

                if (int.TryParse(Console.ReadLine(), out int number))
                {
                    if (_dataVerifier.Verify(hiddenNumber, number, out bool isLess))
                    {
                        message = "Поздравляем, Вы угадали число!";
                        isWin = true;
                    }
                    else
                    {
                        message = $"Неудачная попытка. Введенное число {(isLess ? "меньше" : "больше")} загаданного";
                    }
                }
                else
                {
                    message = "Неверно введено число";
                }

                Console.WriteLine(message);
                Console.WriteLine();

                RaiseOnDoEvent(message);

                step++;

                if (isWin)
                    break;
            }

            message = $"Игра завершилась {(isWin ? "победой" : "неудачей")}";

            Console.WriteLine(message);
            RaiseOnDoEvent(message);
        }
    }
}
