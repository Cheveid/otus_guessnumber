﻿using OTUS_GuessNumber.Interfaces;
using System;
using System.IO;

namespace OTUS_GuessNumber.Implementations
{
    public class FileLogger : ILogger
    {
        private string _filePath;

        public FileLogger(string filePath)
        {
            _filePath = filePath;
        }

        public void Log(string message)
        {
            using (StreamWriter sw = new StreamWriter(_filePath, true, System.Text.Encoding.UTF8))
            {
                sw.WriteLine($"{DateTime.Now.ToShortTimeString()} - {message}");
            }
        }
    }
}
