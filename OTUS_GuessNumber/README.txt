﻿Принцип единственной ответственности
	- SettingsConfiguration - класс для инициализации настроек
	- Game - абстрактный класс игры
	- IDataGenerator - и/ф для генератора числа
	- IDataVerifier - и/ф для проверки введенного числа
	- IGameLauncher - и/ф для запуска игры
	- ILogger - и/ф для логгирования

Принцип открытости/закрытости
	- IGameLauncher - и/ф запускает любую игру, которая будет унаследована от базового класса Game

Принцип подстановки Барбары Лисков
	- класс GuessGame наследуется от абстрактного класса Game, и может быть использован вместо него

Принцип разделения интерфейса
	- IDataGenerator - и/ф для генератора числа
	- IDataVerifier - и/ф для проверки введенного числа
	- IGameLauncher - и/ф для запуска игры
	- ILogger - и/ф для логгирования

Принцип инверсии зависимостей
	- GameLauncher - зависит от абстракций ILogger и Game
	- GuessGame - зависит от абстракций IDataGenerator и IDataVerifier